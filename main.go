package main

import (
	"context"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strconv"
	"time"

	a24 "gitlab.com/hmlkao/active24-go"
)

func main() {
	config := &a24.Configuration{
		Host:   os.Getenv("A24_HOST"),
		Scheme: os.Getenv("A24_SCHEME"),
		// Debug:  true,
	}
	auth := auth(os.Getenv("A24_TOKEN"))
	domain := os.Getenv("A24_DOMAIN")
	subdomain := os.Getenv("A24_SUBDOMAIN")
	period, err := strconv.Atoi(os.Getenv("A24_PERIOD"))
	if err != nil {
		log.Fatalf("fail to get period: %v", err)
	}

	client := a24.NewAPIClient(config)

	log.Println("app is running")

	for {
		time.Sleep(time.Duration(period) * time.Second)

		// Get public IP address
		currentIP, err := getPublicIP()
		if err != nil {
			log.Printf("fail to get public IP: %s", err)
			continue
		}

		// Get current DNS record
		record, _, err := getRecord(client, auth, domain, subdomain, "A")
		if err != nil {
			log.Printf("fail to get record: %s", err)
			continue
		}

		if currentIP != record.Ip {
			// Update backup DNS record
			err := updateRecord(client, auth, domain, "bkp", record.Ip)
			if err != nil {
				log.Printf("fail to update record '%s': %s", "bkp", err)
				continue
			}

			// Update DNS record
			updateRecord(client, auth, domain, subdomain, currentIP)
			if err != nil {
				log.Printf("fail to update record '%s': %s", subdomain, err)
				continue
			}
		} else {
			log.Printf("IP address is still the same - %s", currentIP)
		}
	}
}

func getPublicIP() (string, error) {
	var ip string

	res, err := http.Get("https://api.ipify.org")
	if err != nil {
		return ip, err
	}

	ipb, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return ip, err
	}
	ip = string(ipb)

	return ip, nil
}

func updateRecord(client *a24.APIClient, auth, domain, subdomain, ip string) error {
	log.Printf("update DNS record '%s.%s' with IP %s", subdomain, domain, ip)

	currentRecord, statusCode, err := getRecord(client, auth, domain, subdomain, "A")
	if err != nil {
		if statusCode == 404 {
			log.Printf("DNS record doesn't exist, creating")

			recordCreate := a24.DnsRecordACreate{
				Name: subdomain,
				Ip:   ip,
				Ttl:  300,
			}
			_, err := client.DNSApi.AddNewARecordUsingPOST(context.Background(), auth, domain, recordCreate, &a24.AddNewARecordUsingPOSTOpts{})
			if err != nil {
				return fmt.Errorf("fail to create DNS record: %s", err)
			}
		}

		return err
	}

	recordUpdate := a24.DnsRecordAUpdate{
		HashId: currentRecord.HashId,
		Ip:     ip,
		Name:   subdomain,
		Ttl:    300,
	}

	_, err = client.DNSApi.UpdateARecordUsingPUT(context.Background(), auth, domain, recordUpdate, &a24.UpdateARecordUsingPUTOpts{})
	if err != nil {
		return fmt.Errorf("fail to update DNS record: %s", err)
	}

	return nil
}

func getRecord(client *a24.APIClient, auth, domain, subdomain, t string) (a24.DnsRecord, int, error) {
	var DNSRecord a24.DnsRecord

	DNSRecords, resp, err := client.DNSApi.GetDnsRecordsUsingGET(context.Background(), auth, domain, &a24.GetDnsRecordsUsingGETOpts{})
	if err != nil {
		return DNSRecord, resp.StatusCode, fmt.Errorf("fail to get DNS records: %v", err)
	}
	for _, DNSRecord := range DNSRecords {
		if DNSRecord.Type == t && DNSRecord.Name == subdomain {
			return DNSRecord, resp.StatusCode, nil
		}
	}

	return DNSRecord, 404, fmt.Errorf("no such record")
}

func auth(token string) string {
	return fmt.Sprintf("Bearer %s", token)
}
