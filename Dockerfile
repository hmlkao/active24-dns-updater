FROM golang:1.14.6-alpine3.12 as builder
SHELL ["/bin/sh", "-euxc"]
COPY . /src
WORKDIR /src
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -ldflags '-extldflags "-static"' -o dns-updater /src

FROM alpine:3.12
COPY --from=builder /src/dns-updater /opt/
ENTRYPOINT [ "/opt/dns-updater" ]
