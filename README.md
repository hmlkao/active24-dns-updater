Active24 DNS Updater
====================

Application obtain public IP address and update A DNS record of Active24 provider.

Active24 [documentation](https://faq.active24.com/cz/790131-REST-API-rozhran%C3%AD?l=cs).

Variables
=========

| Variable        | Default | Description
| --------------- | ------- | --------------
| `A24_HOST`      | `""`    | API host, eg. `sandboxapi.active24.com`
| `A24_SCHEME`    | `""`    | API scheme, eg. `https`
| `A24_TOKEN`     | `""`    | API token generated for account
| `A24_DOMAIN`    | `""`    | Domain used for modification
| `A24_SUBDOMAIN` | `""`    | DNS record which will be modified
| `A24_PERIOD`    | `""`    | How often will be subdomain checked (in seconds)
