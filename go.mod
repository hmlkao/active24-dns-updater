module gitlab.com/hmlkao/active24-dns-updater

go 1.13

require (
	github.com/davecgh/go-spew v1.1.1
	gitlab.com/hmlkao/active24-go v0.0.0-20200718131645-155d61fd09d6
)
